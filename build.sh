#!/bin/sh

git submodule update --init
git submodule foreach git checkout main
git submodule foreach npm i
